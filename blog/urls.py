from django.conf.urls import include, url
from django.contrib import admin
from app import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^(\d+)/(\w+)/$', views.index),
    url(r'^list/$', views.listing),
    url(r'^post2db/$', views.post2db),
    url(r'^userinfo/$', views.userinfo),
    url(r'^posting/$', views.posting),
    url(r'^diarybrowsing/$', views.diarybrowsing),
    url(r'^diarybrowsing/(\d+)/(\w+)/$', views.diarybrowsing),
    url(r'^contact/$', views.contact),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^login/$', views.login),
    url(r'^logout/$', views.logout),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
]
