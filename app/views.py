# _*_ encoding:utf-8 _*_
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
from django.template import RequestContext
from django.template import Context, Template
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseRedirect
from app import models, forms
from django.shortcuts import render, HttpResponse, redirect
from django.contrib import auth, messages
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


@login_required(login_url='/login/')
def index(request, pid=None, del_pass=None):
    
    if request.user.is_authenticated():
        username=request.user.username
    
    posts = models.Post.objects.filter(enabled=True).order_by('-pub_time')[:150]


    if del_pass and pid:
        try:
            post = models.Post.objects.get(id=pid)
        except:
            post = None
        
        if post:
            
            if post.del_pass == del_pass:
                post.delete()
                message = "資料刪除成功"
            else:
                message = "密碼錯誤"

    
    return render(request, 'index.html', locals())


@login_required(login_url='/login/')
def listing(request):
    if request.user.is_authenticated():
        username=request.user.username
    template = get_template('listing.html')
    posts = models.Post.objects.filter(enabled=True).order_by('-pub_time')[:150]
    moods = models.Mood.objects.all()

    html = template.render(locals())

    return HttpResponse(html)


@login_required(login_url='/login/')
def post2db(request):
    if request.user.is_authenticated():
        username=request.user.username

    if request.method == 'POST':
        post_form = forms.PostForm(request.POST)
        
        if post_form.is_valid():
            message = "您的訊息已儲存，要等管理者啟用後才看得到喔。"
            post_form.save()
            return HttpResponseRedirect('/list/')
        else:
            message = '如要張貼訊息，則每一個欄位都要填...'
    else:
        post_form = forms.PostForm()
        message = '如要張貼訊息，則每一個欄位都要填...'          

    return render(request, 'post2db.html',locals())


@login_required(login_url='/login/')
def contact(request):
    
    if request.user.is_authenticated():
        username=request.user.username    
    
    if request.method == 'POST':
        form = forms.ContactForm(request.POST)
        
        if form.is_valid():
            message = "感謝您的來信，我們會儘速處理您的寶貴意見。"
            user_name = form.cleaned_data['user_name']
            user_city = form.cleaned_data['user_city']
            user_school = form.cleaned_data['user_school']
            user_email = form.cleaned_data['user_email']
            user_message = form.cleaned_data['user_message']
            subject = "From Blog's Suggestion"
            contact_message = "%s says: %s"%(
                user_name,
                user_message,
                )
            email = EmailMessage(subject,contact_message,user_email,['zj4210214@gmail.com'])

            email.send()
        else:
            message = "請檢查您輸入的資訊是否正確！"
    else:
        form = forms.ContactForm()
    return render(request, 'contact.html', locals())


def login(request):
    
    request.session.set_expiry(600)
    
    if request.method == 'POST':
        login_form = forms.LoginForm(request.POST)
        
        if login_form.is_valid():
            login_name = request.POST['username'].strip()
            login_password = request.POST['password']
            user = authenticate(username=login_name, password=login_password)
            
            if user is not None:
                
                if user.is_active:
                    auth.login(request,user)
                    return redirect('/')
                else:
                    messages.add_message(request, messages.WARNING, 'Account is Inactive!')
            else:
                messages.add_message(request, messages.WARNING, 'Login Failed!')
        else:
            messages.add_message(request, messages.INFO, 'Please Check Input!')
    else:
        login_form = forms.LoginForm()

    return render(request, 'login.html', locals())


def logout(request):
    
    auth.logout(request)
    messages.add_message(request, messages.INFO, 'Logout Successfully')
    return redirect('/')


@login_required(login_url='/login/')
def userinfo(request):
    
    if request.user.is_authenticated():
        username = request.user.username
        try:
            user = User.objects.get(username=username)
            userinfo = models.Profile.objects.get(user=user)
        except:
            pass


    return render(request, 'userinfo.html', locals())



@login_required(login_url='/login/')
def posting(request):
    
    if request.user.is_authenticated():
        username = request.user.username
        useremail = request.user.email
    messages.get_messages(request)
        
    if request.method == 'POST':
        user = User.objects.get(username=username)
        diary = models.Diary(user=user)
        post_form = forms.DiaryForm(request.POST, instance=diary)
        
        if post_form.is_valid():
            post_form.save()  
            return redirect('/diarybrowsing')
        else:
            messages.add_message(request, messages.INFO, '要張貼日記，每一個欄位都要填...')
    else:
        post_form = forms.DiaryForm()
        messages.add_message(request, messages.INFO, '要張貼日記，每一個欄位都要填...')

    return render(request, 'posting.html', locals())


@login_required(login_url='/login/')
def diarybrowsing(request, pid = None, del_pass = None):
    
    if request.user.is_authenticated():
        username = request.user.username
        password = request.user.password
        
        try:
            user = models.User.objects.get(username = username)
            diaries = models.Diary.objects.filter(user=user).order_by('-ddate')
        except:
            pass
            
        if del_pass and pid:
            try:
                diary = models.Diary.objects.get(id=pid)
                diary.delete()
            except:
                diary = None
                message = "can't find diary"
            
    return render(request, 'diarybrowsing.html', locals())

