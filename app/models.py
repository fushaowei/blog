# _*_ encoding: utf-8 _*_
from django.db import models
from django.contrib.auth.models import User


SEX = ((u'female',u'female'),(u'male',u'male'))


class Mood(models.Model):
    status = models.CharField(max_length=10, null=False)

    def __str__(self):
        return self.status

class Post(models.Model):
    mood = models.ForeignKey('Mood', on_delete=models.CASCADE)
    nickname = models.CharField(max_length=10, default='交大金城武(林志玲)')
    message = models.TextField(null=False)
    del_pass = models.CharField(max_length=10)
    pub_time = models.DateTimeField(auto_now=True)
    enabled = models.BooleanField(default=False)
    def __str__(self):
        return self.message



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    age = models.PositiveIntegerField(default=20)
    height = models.PositiveIntegerField(default=160)
    sex = models.CharField(max_length=20,choices=SEX)
    website = models.URLField(null=True)
    def __str__(self):
        return self.user.username



class Diary(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    budget = models.PositiveIntegerField(default=0)
    weight = models.FloatField(default=0)
    note = models.TextField()
    ddate = models.DateField()

    def __str__(self):
        return "{}({})".format(self.ddate, self.user)
